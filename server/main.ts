import express from 'express';
// @ts-ignore
import { loadNuxt, build } from "nuxt";
import cookieParser from "cookie-parser";
import { userRouter } from './routes/users';
import { recipeRouter } from './routes/recipes';
import { authRouter } from './routes/auth';
import { AppDataSource } from './data-source';
import 'reflect-metadata'

const isDev = process.env.NODE_ENV !== 'production';

AppDataSource.initialize()
  .then(async () => {
    console.info("Data Source has been initialized!")
    console.info("Starting server...");

    const app = express();
    app.use(express.json());
    app.use(cookieParser());
    console.info("Registering routes...");


    app.use("/api/users", userRouter);
    app.use("/api/recipes", recipeRouter);
    app.use("/api/auth", authRouter);
    app.use(function (_req, res, next) {
      res.header("Content-Type", "application/json");
      next();
    });
    console.info("Registered routes!");
    console.info("Registering nuxt...");


    const nuxt = await loadNuxt(isDev ? "dev" : "build");


    app.use(nuxt.render);

    if (isDev) {
      build(nuxt)
    }

    app.listen(3000, () => {
      console.log("Server is running on port 3000");
    });
  })
  .catch((err) => {
    console.error("Error during Data Source initialization", err)
  })
