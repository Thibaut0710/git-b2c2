import { Router } from "express";
import { Ingredient } from "../entities/Ingredient";
import { Recipe } from "../entities/Recipe";
import { User } from "../entities/User";
import { IIngredient } from "../models/IIngredient";

export const recipeRouter = Router();

recipeRouter.get("/", (_req, res) => {
    Recipe.find({
        relations: {
            user: true
        }
    }).then(recipes => {
        if (!recipes) {
            res.status(404).send("Recipe not found");
            return;
        }
        res.json(recipes);
    })
})


recipeRouter.get("/:id", (req, res) => {
    Recipe.findOne({
        // @ts-ignore
        id: req.params.id, 
        relations: {
            user: true
        }
    }).then(recipe => {
        if (!recipe) {
            res.status(404).send("Recipe not found");
            return;
        }
        res.send(recipe.toJson());
    })
})


recipeRouter.post("/", (req, res) => {
    const jwt = req.cookies.jwt;
    if (!jwt) {
        res.status(401).send("Unauthorized");
        return;
    }

    // Verify jwt

    User.verifyToken(jwt).then((user) => {

        const { name, ingredients, description, image }: { name: string, description: string, ingredients: IIngredient[], image:string } = req.body;

        if (!name || !ingredients || !description || !image) {
            res.status(400).send("Missing parameters");
            return;
        }

        const recipe = new Recipe();
        recipe.name = name;
        recipe.description = description;
        recipe.user = user;
        recipe.ingredients = ingredients.map(ingredient => {
            return new Ingredient(ingredient.name, ingredient.amount, ingredient.unit);
        });
        recipe.image = image;
        recipe.save().then(recipe => {
            return res.status(201).send(recipe.toJson());
        })

    }).catch((_err) => {
        res.status(401).send("Unauthorized");
    });

});