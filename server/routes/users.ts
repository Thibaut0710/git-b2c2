import { Router } from "express";
import { User } from "../entities/User";

export const userRouter = Router();

userRouter.get("/:id", (req, res) => {

    User.findOne({
        // @ts-ignore
        id: req.params.id
    }).then(user => {
        if(!user) {
            res.status(404).send("User not found");
            return;
        }
        res.send(user.toJson());
    })

})

userRouter.get('/:id/recipes', (req, res) => {
    User.findOne({
        // @ts-ignore
        id: req.params.id,
        relations: {
            recipes: true
        }
    }).then(user => {
        if(!user) {
            res.status(404).send("User not found");
            return;
        }
        return res.status(200).json(user.recipes.map(recipe => recipe.toJson()));
    })
})