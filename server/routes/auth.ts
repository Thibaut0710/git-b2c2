import { Router } from 'express';
import { hashPassword } from '../utils/passwordUtils';
import { User } from '../entities/User';

export const authRouter = Router();

authRouter.post("/login", (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).send("Missing parameters");
        return;
    }
    User.findOne({
        // @ts-ignore
        email
    }).then(user => {
        if (!user) {
            res.status(400).send("User not found");
            return;
        }
        user.login(password).then(token => {
            res.status(200).cookie("jwt", token).send("");
        });
    });
});

authRouter.get("/logout", (_req, res) => {
    res.clearCookie("jwt").send("");
});

authRouter.post("/register", (req, res) => {
    const { name, email, password } = req.body;
    if (!name || !email || !password) {
        res.status(400).send("Missing parameters");
        return;
    }
    User.findOne({
        // @ts-ignore
        email
    }).then(user => {
        if (user) {
            res.status(400).send("User already exists");

        }
    });

    hashPassword(password).then(hash => {
        if (!hash) return res.status(500).send("Internal server error");
        const user = new User();
        user.name = name;
        user.email = email;
        user.hash = hash;

        user.save().then((dbUser) => {
            dbUser.login(password).then(token => {
                res.status(200).cookie("jwt", token, {
                    expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 7),
                }).send("");
            })
        }).catch(_err => {
            res.status(500).send("Internal server error");
        });

    }).catch(_err => {
        res.status(500).send("Internal server error");
    })

});