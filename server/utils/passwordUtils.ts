import bcrypt from 'bcrypt';

export const hashPassword = (password: string): Promise<string> => {
    return new Promise<string>((resolve, reject) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
            if (err) {
                reject(err);
            }
            resolve(hash);
        });
    });
});
}

export const comparePassword = (password: string, hash: string): Promise<boolean> => {
    return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, (err, result) => {
        if (err) {
            reject(err);
        }
        resolve(result);
    });
});
}