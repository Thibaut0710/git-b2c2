import path from 'path';
import { DataSource } from "typeorm"

export const AppDataSource = new DataSource({
    type: "mongodb",
    url: "mongodb://root:root@localhost:27017/?authSource=admin",
    database: "project_git",
    synchronize: true,
    entities: [
        path.join(__dirname, "/entities/**/*.ts")
    ],
});

