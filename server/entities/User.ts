import { Entity, Column, BaseEntity, OneToMany, ObjectID, ObjectIdColumn } from 'typeorm';
import { ObjectId } from 'mongodb';
import jwt from 'jsonwebtoken';
import { IUser } from '../models/IUser';
import { comparePassword } from '../utils/passwordUtils';
import { Recipe } from './Recipe';

@Entity()
export class User extends BaseEntity implements IUser {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    name: string

    @Column()
    email: string

    @Column()
    hash: string

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @OneToMany(type => Recipe, recipe => recipe.user)
    recipes: Recipe[];


    public toJson() {
        return JSON.stringify({
            id: this.id,
            name: this.name,
            recipes: this.recipes.map(recipe => recipe.id)
        });
    }

    public login(password: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            comparePassword(password, this.hash).then(isValid => {
                if (!isValid) {
                    reject(new Error("Invalid password"));
                    return;
                }
                const token = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (60 * 60),
                    data: { id: this.id }
                }, process.env.JWT_SECRET!);
                resolve(token);
            });
        });
    }

    public static verifyToken(token: string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            jwt.verify(token, process.env.JWT_SECRET!, (err, decoded) => {
                if (err) {
                    reject(err);
                    return;
                }

                User.findOne({
                    // @ts-ignore
                    _id: new ObjectId(decoded.data.id)
                }).then(user => {
                    if (!user) {
                        reject(new Error("User not found"));
                        return;
                    }
                    resolve(user);
                }).catch(err => {
                    reject(err);
                });
            })
        })
    }


}