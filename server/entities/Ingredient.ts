import { Column } from 'typeorm';
import { IIngredient } from '../models/IIngredient';

export class Ingredient implements IIngredient {

    @Column()
    name: string;

    @Column()
    amount: number;

    @Column()
    unit: string;


    constructor(name: string, amount: number, unit: string) {
        this.name = name;
        this.amount = amount;
        this.unit = unit;
    }

    public toJson() {
        return JSON.stringify({
            name: this.name,
            amount: this.amount,
            unit: this.unit,
        });
    }

}