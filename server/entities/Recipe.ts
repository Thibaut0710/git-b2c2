import { Entity, Column, BaseEntity, ManyToOne, ObjectID, ObjectIdColumn } from 'typeorm';
import { IRecipe } from '../models/IRecipe';
import { Ingredient } from './Ingredient';
import { User } from './User';

@Entity()
export class Recipe extends BaseEntity implements IRecipe {

    @ObjectIdColumn()
    id: ObjectID

    @Column()
    name: string

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @ManyToOne(() => User, user => user.recipes)
    user: User;
    
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    @Column((type) => Ingredient)
    ingredients: Ingredient[];

    @Column()
    description: string;

    @Column()
    image: string

    public toJson() {
        return JSON.stringify({
            id: this.id,
            name: this.name || "",
            user: this.user?.id || "",
            ingredients: this.ingredients,
            description: this.description || "",
            image: this.image || ""
        });
    }

}