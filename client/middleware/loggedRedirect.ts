// @ts-nocheck
export default function ({ route, redirect, app }) {
  if ((route.name === 'login' || route.name === "register") && app.$cookies.get('jwt')) {
    return redirect('/')
  }
  if (route.name === "recipes-create" && !app.$cookies.get('jwt')) {
    return redirect('/login')
  }


}