// @ts-nocheck
/* eslint-disable prefer-promise-reject-errors */

import { IRecipe } from "~/server/models/IRecipe";

export const state = () => {
    return {
        users: [],
        recipes: [],
    }
}

export const mutations = {
    addUser(state, user) {
        if (state.users.find(u => u.id === user.id)) return;
        state.users.push(user);
    },
    addRecipe(state, recipe) {
        if (state.recipes.find(r => r.id === recipe.id)) return;
        state.recipes.push(recipe);
    }
}

export const actions = {
    getRecipes({ commit, state }) {
        return new Promise<IRecipe[]>((resolve, _reject) => {
            this.$http.get("/api/recipes/")
                .then((response) => response.json())
                .then((data: Array<unknown>) => {
                    data.forEach(recipe => {
                        commit("addRecipe", recipe);
                    })
                    resolve(state.recipes);
                })
        })
    },
    getRecipe({ commit, state }, id) {
        return new Promise<>((resolve, _reject) => {

            const recipe = state.recipes.find(recipe => recipe.id === id);
            if (recipe) return resolve(recipe);
            this.$http.get("/api/recipes/" + id)
                .then(response => response.json())
                .then(data => {
                    commit("addRecipe", data);
                    resolve(state.recipes.find(recipe => recipe.id === data.id));
                })
        })
    },
    getUser({ commit, state }, id) {
        return new Promise<>((resolve, _reject) => {

            const user = state.users.find(user => user.id === id);
            if (user) return resove(user);
            this.$http.get("/api/users/" + id)
                .then(response => response.json())
                .then(data => {
                    commit("addUser", data);
                    resolve(state.recipes.find(user => user.id === id));
                })
        })
    },
    getUserRecipes({ commit, state }, id) {
        return new Promise<>((resolve, reject) => {
            this.$http.get(`/api/users/${id}/recipes`,
                {
                    method: "GET",
                }).then(response => {
                    if (response.status !== 200) {
                        reject("Error getting user recipes");
                    }
                    response.json().then((data: Array<unknown>) => {
                        data.forEach(recipe => {
                            commit("addRecipe", recipe);
                        });
                        resolve(state.recipes.filter(recipe =>
                            recipe.userId === id
                        ))
                    })
                })
        })
    },
    createRecipe({ commit, state }, { name, description, ingredients, image }) {
        return new Promise((resolve, reject) => {
            if (!name || !description || !ingredients) {
                reject("Missing args");
            }

            this.$http.post("/api/recipes",
                {
                    name, description, ingredients, image
                }).then(response => {
                    if (response.status !== 201) {
                        reject("Error while creating recipe");
                    }
                    response.json().then(data => {
                        commit("addRecipe", data);
                        resolve(state.recipes.find(recipe => recipe.id === data.id))
                    })
                })

        })
    }
}