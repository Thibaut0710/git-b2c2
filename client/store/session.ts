// @ts-nocheck
export const state = () => {
    return {
        user: null,
        token: null
    }
}

export const mutations = {
    setToken(state, token) {
        state.token = token;
        this.$router.push('/');
    }
}

export const actions = {
    nuxtServerInit({ commit }, context) {
        const { req } = context;
        if (req.headers.cookie) {
            const jwtCookie = req.headers.cookie.split(";").find(c => c.trim().startsWith("jwt="));
            if (!jwtCookie) return;
            const token = jwtCookie.split("=")[1];
            commit("setToken", token);
        }
    },
    login({ commit }, { email, password }) {
        return new Promise((resolve, reject) => {
            fetch("/api/auth/login", {
                method: "POST",
                body: JSON.stringify({ email, password }),
                headers: { "Content-Type": "application/json" },
                credentials: 'same-origin'
            }).then(response => {
                if (response.status !== 200) {
                    reject(new Error("Failed to login"));
                }
                const jwt = this.$cookies.get("jwt");
                commit("setToken", jwt);
                resolve();
            });
        });
    },
    register({ commit }, { name, email, password }) {
        fetch("/api/auth/register", {
            method: "POST",
            body: JSON.stringify({ name, email, password }),
            headers: { "Content-Type": "application/json" },
            credentials: 'same-origin'
        }).then(response => {
            if (response.status !== 200) {
                throw new Error("Error registering user");
            }
            const jwt = this.$cookies.get("jwt");
                commit("setToken", jwt);
                resolve();
            // response.headers.get("cookie")?.split(";").forEach(cookie => {
            //     if (cookie.trim().startsWith("jwt")) {
            //         commit("setToken", cookie.split("=")[1]);
            //     }
            // });
        });
    }
}