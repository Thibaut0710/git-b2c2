import { IIngredient } from "./IIngredient";
import { IUser } from "./IUser";

export interface IRecipe {
    name: string;
    user: IUser;
    ingredients: IIngredient[];
    description: string;
    image: string;
}