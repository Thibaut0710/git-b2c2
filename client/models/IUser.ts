import { IRecipe } from "./IRecipe";

export interface IUser {
    name: string;
    email: string;
    hash: string;
    recipes: IRecipe[];
}