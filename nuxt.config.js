export default {
  head: {
    title: 'projet_git',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  srcDir: 'client/',
  css: [],
  plugins: [],
  components: true,
  router: {
    middleware: 'loggedRedirect',
  },
  buildModules: ['@nuxt/typescript-build', '@nuxtjs/tailwindcss'],
  modules: ['cookie-universal-nuxt', '@nuxtjs/axios', '@nuxt/http'],
  typeorm: {
    type: 'mongodb',
    host: 'localhost',
    port: '27017',
    database: 'projet_git',
    url: 'mongodb://root:root@localhost:27017/?authMechanism=DEFAULT&authSource=admin',
    entities: ['./api/entities/**/*.ts'],
  },
  axios: {
    baseURL: 'http://localhost:3000/api',
  },
  build: {},
}
